
package cadastro;



/**
 *
 * @author matheus
 */
import Conexao.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import Usuario.Usuario;

public class Cadastro {

    Connection con;
    
    public Cadastro() {
        con = Conexao.getConnection();
    }
    

 public void create(Usuario u) {

        PreparedStatement stmt = null;

        try {
           stmt = con.prepareStatement( "Insert into usuario (login,senha,nome,saldo)Values (?,?,?,0)");
            
            stmt.setString(1,u.getLogin());
            stmt.setString(2,u.getSenha());
              stmt.setString(3,u.getNome());
             stmt.executeUpdate();

            

            JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "erro ao salva!"+ex);
        } finally {
            Conexao.closeConnection(con, stmt);
        }
 }

    
}
    